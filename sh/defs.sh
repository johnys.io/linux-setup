export GREEN="\e[32m"
export RED="\033[31m"
export YELLOW="\e[33m"
export CLOSE="\e[0m"

export TIK="$GREEN✔$CLOSE"
export CROSS="$RED✘$CLOSE"

print_success() {
	if [ -z "$2" ]; then
		echo -e "	>>$YELLOW [$1]$GREEN Succeeded! $TIK"
	else 
		echo -e "	>>$YELLOW [$1]$GREEN $2! $TIK"
	fi
}
export -f print_success

print_failure() {
    echo -e "   >> $1 $2 $CROSS"
}
export -f print_failure

print_installing() {
	echo -e ">>>$YELLOW [$1]$CLOSE Installing..."
}
export -f print_installing
