#!/usr/bin/env bash
PROGRAM=PROGRAMS
print_installing "$PROGRAM"

declare -a SCRIPTS

SCRIPTS=( packages vim-plugins oh-my-zsh docker docker-compose google-chrome )

for SCRIPT in "${SCRIPTS[@]}";
do
	echo ""
	echo -e "	>>$YELLOW [$SCRIPT]$CLOSE Starting..."
	`dirname "$0"`"/""$SCRIPT"".sh" && print_success "$SCRIPT" || print_failure "$SCRIPT"
done
