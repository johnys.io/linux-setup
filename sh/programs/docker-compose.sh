#!/usr/bin/env bash
PROGRAM=docker-compose
print_installing "$PROGRAM"

sudo apt install curl
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
