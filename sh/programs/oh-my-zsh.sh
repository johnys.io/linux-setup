#!/usr/bin/env bash
PROGRAM=oh-my-zsh
print_installing "$PROGRAM"

wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh

chsh -s `which zsh` && print_success "$PROGRAM" "Changed shell!
