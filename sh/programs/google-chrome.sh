#!/usr/bin/env bash
PROGRAM=google-chrome
print_installing "$PROGRAM"

sudo apt install -y libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O /tmp/chrome.deb
sudo dpkg -i /tmp/chrome.deb
sudo apt install -fy
sudo dpkg -i /tmp/chrome.deb && print_success "$PROGRAM"
