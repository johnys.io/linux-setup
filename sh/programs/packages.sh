#!/usr/bin/env bash
PROGRAM="apt-get packages"
print_installing "$PROGRAM"

declare -a PACKAGES
PACKAGES=( wget curl vim zsh git )

for PACKAGE in "${PACKAGES[@]}";
do
	echo ""
	print_installing "$PACKAGE"
	sudo apt install -y "$PACKAGE" && print_success "$PACKAGE"
done
