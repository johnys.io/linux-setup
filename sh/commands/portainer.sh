#!/usr/bin/env bash
COMMAND=portainer
print_installing "$COMMAND"

read -d '' SCRIPT <<"BLOCK"
if [ $1 = 'up' ] || [ $1 = 'start' ]; then
	docker run -d -p 9000:9000 --name portainer_admin -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
elif [ $1 = 'down' ] || [ $1 = 'stop' ]; then
	docker container stop portainer_admin
elif [ $1 = 'rm' ]; then
	docker container rm -f portainer_admin
else
	echo "Commands: [up, down, rm]"
fi
BLOCK

echo "$SCRIPT" | sudo tee /usr/local/bin/portainer && sudo chmod +x /usr/local/bin/portainer
