#!/usr/bin/env bash
COMMAND=composer
print_installing "$COMMAND"

read -d '' SCRIPT <<"BLOCK"
#!/bin/sh
 export PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
 echo "Current working directory: '"$(pwd)"'"
 docker run --rm -v $(pwd):/app -v ~/.ssh:/root/.ssh composer/composer $@
BLOCK

echo "$SCRIPT" | sudo tee /usr/local/bin/composer && sudo chmod +x /usr/local/bin/composer
